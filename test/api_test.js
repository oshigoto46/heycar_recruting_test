//https://www.npmjs.com/package/supertest

var supertest = require("supertest");
var should = require("should");

var server = supertest.agent("http://localhost:3000");


// test case #1 
//  --->  /search
describe("search api endpoint test --> /search",function(){
	  it("should return json page and contains some properties",function(done){
		      server
		      .get("/search")
		      .expect("Content-type",/json/)
		      .expect(200) // THis is HTTP response
		      .end(function(err,res){
	                    //console.log(res.body[0]);		            
		            res.status.should.equal(200);
			    res.body[0].should.have.property("consistent_code");
			    res.body[0].should.have.property("make");
			    res.body[0].should.have.property("model");
			    res.body[0].should.have.property("power");
			    res.body[0].should.have.property("year");
			    res.body[0].should.have.property("color");
			    res.body[0].should.have.property("price");
		            //res.body.error.should.equal(false);
		            done();
		          });
		    });

});


/*
describe('POST /search_by_item', function() {
	  it('user.name should be an case-insensitive match for "john"', function(done) {
		        server
			.post('/search_by_item')
		        .send('make=audi') // x-www-form-urlencoded upload
		        .set('Accept', 'application/json')
		        .expect(200, { 
			        	consistent_code: '2',
				        model: 'a3',
				        power :'111',
				        year :'2016',
				        color :'white',
				        price :'17210'
				      }, done);
		    });
});
*/


// test case #2
// --> /search_by_item
describe("posting search_by_item ---> POST /search_by_item",function(){
	  it("should search by item",function(done){
		      server
		      .post("/search_by_item")
		      .send({"make" : "audi"})
		      .expect(200) // THis is HTTP response
		      .end(function(err,res){
	                    console.log(res.body);	// no return ...	            
	                    //console.log(res);		            
	                    //console.log(err);		            
		            res.status.should.equal(200);
		            //res.body.error.should.equal(false);
		            done();
		          });
		    });

});


// test case #3
// /upload_csv
describe(" POST /search_seller_id with seller_id 122 and Redircet to /upload_csv/122 ",function(){
	  it("should return json page and contains some properties",function(done){
		      server
		      .post("/search_seller_id/")
		      .send({"seller_id": "122"})
		      .expect(302)
		      .expect('Location','/upload_csv/122',done);
		    });

});

/*
describe("search api endpoint test",function(){
	  it("should return json page and contains some properties",function(done){
		      server
		      .get("/")
		      .expect("Content-type",/json/)
		      .expect(200) // THis is HTTP response
		      .end(function(err,res){
	                    //console.log(res.body[0]);		            
		            res.status.should.equal(200);
			    res.body[0].should.have.property("consistent_code");
			    res.body[0].should.have.property("make");
			    res.body[0].should.have.property("model");
			    res.body[0].should.have.property("power");
			    res.body[0].should.have.property("year");
			    res.body[0].should.have.property("color");
			    res.body[0].should.have.property("price");
		            //res.body.error.should.equal(false);
		            done();
		          });
		    });

});
*/
