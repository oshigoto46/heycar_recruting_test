var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var mysql = require('mysql');
var index = require('./routes/index');
var upload = require('./routes/upload');
var vehicle_listings = require('./routes/vehicle_listings');
var search = require('./routes/search');
var search_by_item = require('./routes/search_by_item');
var search_seller_id = require('./routes/search_seller_id');

var app = express();
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', index);
//app.use('/new', add);
//app.use('/edit', edit);
//app.use('/delete', del);
app.use('/upload_csv', upload);
app.use('/vehicle_listings', vehicle_listings);
app.use('/search', search);
app.use('/search_by_item', search_by_item);
app.use('/search_seller_id', search_seller_id);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
  //res.redirect('/');
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
  //res.redirect('/');

});


module.exports = app;
