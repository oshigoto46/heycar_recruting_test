var express = require('express');
var router = express.Router();
var mysql = require('mysql');



router.get('/', function(req, res, next) {
   res.render('index');
});

// for api access
router.get('/search', function(req, res, next) {

  var connection = mysql.createConnection({
    host: 'localhost' , user: 'root',  password: 'P@ssw0rd',  database: 'my_db', port: 3306,   dateStrings: true
  });
  var sql = 'SELECT * FROM car_stock_table ORDER BY consistent_code ASC;'

  connection.query(sql, function(error, results, fields) {
    if (error) throw error;
    res.json(results);
  });
});


module.exports = router;
