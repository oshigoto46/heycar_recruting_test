
var express = require('express');
var router = express.Router();

// DB接続定義
var mysql = require('mysql');
var connection = mysql.createConnection({
    host: 'localhost' , user: 'root',  password: 'P@ssw0rd',  database: 'my_db', port: 3306,   dateStrings: true
});
 
/* 一覧画面表示処理 */
router.post('/', function(req, res, next) {
  // メモ一覧をDBから取り出してsampleMemosに格納
  var carQuerySellerId  = req.body.seller_id;
  var sql = 'SELECT dealer_id FROM dealer_table WHERE ?' ;
  
  connection.query(sql,{dealer_id: carQuerySellerId}, function(error, rows) {
   for(v of rows){
	  if(v.dealer_id) {
            res.redirect('/upload_csv/'+carQuerySellerId);
	  }
	  else{
            res.render("no such dealer user");
            res.redirect("/error",{message:"no such dealer user"});
	  }
   }
    //if (error) throw error;
    //res.json(results);
  });
});



module.exports = router;
