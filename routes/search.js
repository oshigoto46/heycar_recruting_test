var express = require('express');
var router = express.Router();

// DB接続定義
var mysql = require('mysql');

/* 一覧画面表示処理 */
router.post('/', function(req, res, next) {

  var sql = 'SELECT * FROM car_stock_table ORDER BY consistent_code ASC;'
  var connection = mysql.createConnection({
    host: 'localhost' , user: 'root',  password: 'P@ssw0rd',  database: 'my_db', port: 3306,   dateStrings: true
  });

  connection.query(sql, function(error, results, fields) {
    if (error) throw error;
    res.json(results);
  });
});


module.exports = router;
