var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var connection = mysql.createConnection({
    host: 'localhost' , user: 'root',  password: 'P@ssw0rd',  database: 'my_db', port: 3306,   dateStrings: true
});

router.get('/', function (req, res) {

	  //throw ("HOGE");
	  res.send('only POST usage')
})


router.post('/', function(req, res) {
	for (let v of req.body){
	     const _data  = { 'consistent_code' : v.code,
		              'make'  : v.make,
		              'model' : v.model,
		              'power' : v.kW,
		              'year'  : v.year,
		              'color' : v.color,
		              'price' : v.price,
		            };

	        var already_exist_flg =false;
	        const promise = new Promise((resolve, reject) => {

		        const sql = 'SELECT * FROM car_stock_table WHERE consistent_code = ? ;'       
	                connection.query(sql,[v.code],function(err,result,fields){
                          for(let v of result){
                             already_exist_flg =true;
	                     //console.log("----------------SELECT(find and update)---------------------------" + already_exist_flg);
			     resolve();
			   }
	                    //console.log("----------------SELECT(not ind and insert)---------------------------" + already_exist_flg);
                            resolve();			     
	                 });
	         });
                promise.then(
	         function(){
	             if(!already_exist_flg){
                       console.log("----------------INSERT-------------------------");
                       let query = 'INSERT INTO car_stock_table SET ?';
    	               connection.query(query ,_data,
	                  function(error,result,fields){
	 	                if(error) {console.log(error);}
	                        //console.log(result);
	                        res.send("insert compelte");
	                   });
		     }
	             if(already_exist_flg){
                        console.log("----------------UPDATE---------------------------");
			var _q = 'UPDATE car_stock_table SET make= ?, model = ? ,power = ? ,year=? , color=? , price=?  WHERE consistent_code= ? ';
                        connection.query(_q,[v.make,v.model,v.kW,v.year,v.color,v.price,v.code], 
				 function (err, result, fields) {
	                               if (err) throw err;
	                               //console.log(result);
				       res.send("update complete");
		         });
			 
		     }
	         });
                	
             }
   }
)

module.exports = router;
